﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Windows.Storage.FileProperties;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Test
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    
    public sealed partial class PhotoPage : Page
    {
        //Automatically flip photos
        private void Flip_Photos()
        {
            
            int change = 1;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += (o, a) =>
            {
                // If we'd go out of bounds then reverse
                
                int newIndex = FlipView.SelectedIndex + change;
                if (newIndex >= FlipView.Items.Count || newIndex < 0)
                {
                    change *= -1;
                }
                if(flip_status == Status.On)
                    FlipView.SelectedIndex += change;
            };
            timer.Start();
        }
        private void FlipView_Loaded(object sender, RoutedEventArgs e)
        {
            //Remove the button 
            Grid grid = (Grid)VisualTreeHelper.GetChild(FlipView, 0);
            for (int i = grid.Children.Count - 1; i >= 0; i--)
                if (grid.Children[i] is Button)
                    grid.Children.RemoveAt(i);
        }
        private async void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }
        private async void Check_Videos(object sender, SelectionChangedEventArgs e)
        {
            int Index = FlipView.SelectedIndex ;
            Grid grid = (Grid)VisualTreeHelper.GetChild(FlipView, 0);

            //Check whether the index is out of range
            if(!(Index >= 0 && Index < FlipView.Items.Count))
            {
                return;
            }
            String source = AllResources[Index].ToString();

            //Check videos and automatically add the videos
            if (source.Contains("mp4"))
            {
                MediaElement video = new MediaElement();
                video.Source = new Uri(this.BaseUri,source);
                video.Play();
                video.AreTransportControlsEnabled = true;
                video.MediaEnded += media_MediaEnded;
                grid.Children.Add(video);
                flip_status = Status.Off;
            }
            else
            {
                //Check whether the video is on the grid
                if(flip_status == Status.Off)
                {
                    //Remove the video 
                    grid.Children.RemoveAt(1);
                    flip_status = Status.On;
                }
            }
        }
        private async void media_MediaEnded(object sender, RoutedEventArgs e)
        {
            // Do your stuff
            MediaElement video = sender as  MediaElement;
            Grid grid = (Grid)VisualTreeHelper.GetChild(FlipView, 0);
            flip_status = Status.On;
            grid.Children.Remove(video);         
        }

        //Let photos fit the screen size
        private void Full_screen()
        {
            FlipView.Width = Window.Current.Bounds.Width;
            FlipView.Height = Window.Current.Bounds.Height;
        }
        private void Set_Images()
        {
            //For local data version
            Images = new ObservableCollection<string>()
            {
                "https://images.pexels.com/photos/18396/pexels-photo.jpg?cs=srgb&dl=couple-engagement-kissing-18396.jpg&fm=jpg",
                "/Assets/PhotoGallery/Picture1.jpg",
                "/Assets/PhotoGallery/Picture2.jpg",
                "/Assets/PhotoGallery/Picture3.jpg",
                "/Assets/PhotoGallery/Picture4.jpg",
                "/Assets/PhotoGallery/Picture5.jpg",
                "/Assets/PhotoGallery/Picture6.jpg",
                "/Assets/PhotoGallery/Picture7.jpg"
            };

            //For Url version
            Images = new ObservableCollection<string>()
            {
                "http://sample.dentall.io/content/images/Picture1.jpg",
                "http://sample.dentall.io/content/images/Picture2.jpg",
                "http://sample.dentall.io/content/images/Picture3.jpg",
                "http://sample.dentall.io/content/images/Picture4.jpg",
                "http://sample.dentall.io/content/images/Picture5.jpg",
                "http://sample.dentall.io/content/images/Picture6.jpg",
                "http://sample.dentall.io/content/images/Picture7.jpg"
            };
        }
        private void Set_Videos()
        {
            Videos = new ObservableCollection<string>()
            {
                "Assets/Video/view.mp4"
            };
        }
        private void Set_AllResources()
        {
            AllResources = new ObservableCollection<string>(Images.Concat(Videos).ToList());
        }
        public ObservableCollection<string> Images { get; private set; }
        public ObservableCollection<string> Videos { get; private set; }
        public ObservableCollection<string> AllResources { get; private set; }

        //Able to flip photo 
        Status flip_status;
        enum Status
        {
            On,
            Off,
        }
        public PhotoPage()
        {
            //Initialization
            this.InitializeComponent();
            flip_status = Status.On;
            Set_Images();
            Set_Videos();
            Set_AllResources();

            //Automatically Flip Photo
            Flip_Photos();

            //Subscribe Size Change Events
            // Window.Current.SizeChanged += (sender, args) => { Full_screen(); };
        }

    }
}
